package com.example.simplerest.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/hello2")
public class HelloController2 {
	
	@GetMapping("/getHello")
	public String getHello() {
		return "(HELLO 2) This is simple Restful API V4 on branch master";
	}
	
}
